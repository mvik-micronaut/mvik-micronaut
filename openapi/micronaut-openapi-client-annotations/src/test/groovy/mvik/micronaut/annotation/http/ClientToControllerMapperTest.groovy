package mvik.micronaut.annotation.http

import io.micronaut.annotation.processing.test.AbstractTypeElementSpec
import io.micronaut.inject.BeanDefinition
import io.micronaut.openapi.visitor.AbstractOpenApiVisitor
import io.swagger.v3.oas.models.OpenAPI
import spock.util.environment.RestoreSystemProperties

@RestoreSystemProperties
class ClientToControllerMapperTest extends AbstractTypeElementSpec {

    def 'generate openapi spec based on client annotation'() {
        given:
            System.setProperty(AbstractOpenApiVisitor.ATTR_TEST_MODE, 'true')
        when:
            BeanDefinition definition = buildBeanDefinition(
                "test.MyBean",
                // language=java
                '''                
                package test;
                
                import io.micronaut.http.annotation.*;
                import io.micronaut.http.client.annotation.Client;
                
                import java.util.Map;
                
                @Client(id = "test-service")
                interface MyClient {
                
                    @Get("/api/persons")
                    Map persons();
                
                    @Post("/api/persons")
                    Map persons(Map person);
                
                }
                
                @javax.inject.Singleton
                class MyBean {}
                '''.stripIndent()
            )


        then: 'the state is correct'
            AbstractOpenApiVisitor.testReference != null

        when: 'the /api/persons path is retrieved'
            OpenAPI openAPI = AbstractOpenApiVisitor.testReference
        then:
            println openAPI
            openAPI.paths['/api/persons'].get
            openAPI.paths['/api/persons'].post
    }
}


