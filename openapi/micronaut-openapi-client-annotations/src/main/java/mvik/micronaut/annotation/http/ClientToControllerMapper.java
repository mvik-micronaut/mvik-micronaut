package mvik.micronaut.annotation.http;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.inject.Singleton;

import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.DefaultScope;
import io.micronaut.core.annotation.AnnotationValue;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.UriMapping;
import io.micronaut.inject.annotation.NamedAnnotationMapper;
import io.micronaut.inject.visitor.VisitorContext;
import io.micronaut.validation.Validated;

/**
 * A {@link NamedAnnotationMapper} that maps the client annotation as a controller annotation.
 *
 * @author mvik
 * @since 1.0
 */
public class ClientToControllerMapper implements NamedAnnotationMapper {

    @Nonnull @Override public String getName() {
        return "io.micronaut.http.client.annotation.Client";
    }

    @Override public List<AnnotationValue<?>> map(AnnotationValue<Annotation> annotation, VisitorContext visitorContext) {

        List<AnnotationValue<?>> annotations = new ArrayList<>(5);

        annotations.add(AnnotationValue.builder(Bean.class).build());
        annotations.add(AnnotationValue.builder(DefaultScope.class).value(Singleton.class).build());
        annotations.add(AnnotationValue.builder(Validated.class).build());

        Optional<String> path = annotation.get("path", String.class);
        annotations.add(AnnotationValue.builder(Controller.class).value(path.orElse("/")).build());
        annotations.add(AnnotationValue.builder(UriMapping.class).value(path.orElse("/")).build());
        return annotations;
    }
}
