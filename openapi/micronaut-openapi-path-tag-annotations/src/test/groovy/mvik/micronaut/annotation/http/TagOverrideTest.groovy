package mvik.micronaut.annotation.http

import spock.lang.Specification
import spock.lang.Unroll

class TagOverrideTest extends Specification {

    @Unroll
    def 'tag override #input -> pathPart=#pathPart and tag=#tag'() {
        when:
            TagOverride tagOverride = TagOverride.of(override)
        then:
            tagOverride.pathPart == pathPart
            tagOverride.tag == tag
        where:
            override         | pathPart    | tag
            'something'      | override    | override
            'something:else' | 'something' | 'else'
    }
}
