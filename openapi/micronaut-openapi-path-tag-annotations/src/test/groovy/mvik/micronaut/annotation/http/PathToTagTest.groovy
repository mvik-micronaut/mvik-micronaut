package mvik.micronaut.annotation.http

import io.micronaut.inject.visitor.VisitorContext
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.environment.RestoreSystemProperties

@RestoreSystemProperties
@SuppressWarnings("GroovyAccessibility")
class PathToTagTest extends Specification {

    VisitorContext context = Mock() {
        get(_, _) >> Optional.empty()
    }

    @Unroll
    def 'extract tag from path #path -> #tag'() {
        when:
            String partToTag = AbstractPathToTagMapper.findPathPartToTag(path, context)
        then:
            partToTag == expected
        where:
            path                            | expected
            '/api/customers'                | 'customers'
            '/api/customers/{customerNo}'   | 'customers'
            '/api/trips/{tripNo}/shipments' | 'trips'
            '/api/logistic-units'           | 'logistic-units'
            '/nested/api/logistic-units'    | 'logistic-units'
            '/api/shipments{?queryParams*}' | 'shipments'
            '/customers'                    | null
            '/ape/customers'                | null
    }

    def 'configurable marker'() {
        given:
            System.setProperty(AbstractPathToTagMapper.MICRONAUT_OPENAPI_PATH_TAG_MARKER, "api/v1")
        when:
            String partToTag = AbstractPathToTagMapper.findPathPartToTag("/api/v1/logistic-units", context)
        then:
            partToTag == 'logistic-units'
    }

    @Unroll
    def 'configure fallbacks from path# -> #tag'() {
        given:
            System.setProperty(AbstractPathToTagMapper.MICRONAUT_OPENAPI_PATH_TAG_MARKER, "api")
            System.setProperty(AbstractPathToTagMapper.MICRONAUT_OPENAPI_PATH_TAG_OVERRIDES, "proxy,webhook")
        when:
            String partToTag = AbstractPathToTagMapper.findPathPartToTag(path, context)
        then:
            partToTag == expected
        where:
            path               | expected
            '/api/customers'   | 'customers'
            '/proxy/customers' | 'proxy'
            '/api/webhook'     | 'webhook'
            '/customers'       | null
    }
}
