package mvik.micronaut.annotation.http

import io.micronaut.annotation.processing.test.AbstractTypeElementSpec
import io.micronaut.openapi.visitor.AbstractOpenApiVisitor
import io.swagger.v3.oas.models.OpenAPI
import spock.lang.Unroll
import spock.util.environment.RestoreSystemProperties

@RestoreSystemProperties
@SuppressWarnings("GroovyAccessibility")
class ControllerPathToTagMapperTest extends AbstractTypeElementSpec {

    @Unroll
    def 'add tags based on controller path [#path] -> [#expectedTag]'() {
        given:
            System.setProperty(AbstractOpenApiVisitor.ATTR_TEST_MODE, 'true')
            System.setProperty(AbstractPathToTagMapper.MICRONAUT_OPENAPI_PATH_TAG_MARKER, "api")
        when:
            buildBeanDefinition('test.MyBean', source)
            OpenAPI openAPI = AbstractOpenApiVisitor.testReference
        then:
            openAPI.paths[resolvedPath].get.tags.contains expectedTag

        where:
            path                             | resolvedPath    | expectedTag
            '/api/persons'                   | path            | 'persons'
            'api/persons'                    | path            | 'persons'
            '/ignored/path/api/persons'      | path            | 'persons'
            '/api/wip-persons/trailing/path' | path            | 'wip-persons'
            '${some.property:/api/contacts}' | '/api/contacts' | 'contacts'

            source = """    
                package test;
                
                import io.micronaut.http.annotation.*;
                import java.util.List;
                
                @Controller("${path}")
                interface MyClient {
                
                    @Get
                    List persons();
                }
                
                @javax.inject.Singleton
                class MyBean {}
                """.stripIndent()

    }

    @Unroll
    def 'add tags based on controller path with overrides [#path] -> [#expectedTag]'() {
        given:
            System.setProperty(AbstractOpenApiVisitor.ATTR_TEST_MODE, 'true')
            System.setProperty(AbstractPathToTagMapper.MICRONAUT_OPENAPI_PATH_TAG_OVERRIDES, overrides)
        when:
            buildBeanDefinition('test.MyBean', source)
            OpenAPI openAPI = AbstractOpenApiVisitor.testReference
        then:
            openAPI.paths[resolvedPath].get.tags.contains expectedTag

        where:
            path                        | overrides             | resolvedPath | expectedTag
            '/api/persons'              | ''                    | path         | 'persons'
            '/ignored/path/api/persons' | 'ignored'             | path         | 'ignored'
            '/ignored/path/api/persons' | 'path,ignored'        | path         | 'path'
            '/ignored/path/api/persons' | 'ignored/path:mapped' | path         | 'mapped'

            source = """    
                package test;
                
                import io.micronaut.http.annotation.*;
                import java.util.List;
                
                @Controller("${path}")
                interface MyClient {
                
                    @Get
                    List persons();
                }
                
                @javax.inject.Singleton
                class MyBean {}
                """.stripIndent()

    }

    def 'generate openapi spec and add tags based on controller path with placeholders (ignored)'() {
        given:
            System.setProperty(AbstractOpenApiVisitor.ATTR_TEST_MODE, 'true')
        when:
            buildBeanDefinition('test.MyBean', source)
            OpenAPI openAPI = AbstractOpenApiVisitor.testReference
        then:
            openAPI.paths['/api/persons'].get.tags.contains 'persons'
            openAPI.paths['/api/persons/{personNo}'].get.tags.contains 'persons'
        where:
            // language=java
            source = '''                
                package test;
                
                import io.micronaut.http.annotation.*;
                
                import java.util.List;
                import java.util.Map;
                
                @Controller
                interface MyClient {
                
                    @Get("${some.ignored.property:/api/persons}")
                    List persons();
                
                    @Get("${some.ignored.property:/api/persons/{personNo}}")
                    Map persons(@QueryValue String personNo);
                
                }
                
                @javax.inject.Singleton
                class MyBean {}
                '''.stripIndent()

    }

    def 'generate openapi spec and add tags based on method path'() {
        given:
            System.setProperty(AbstractOpenApiVisitor.ATTR_TEST_MODE, 'true')
        when:
            buildBeanDefinition('test.MyBean', source)
            OpenAPI openAPI = AbstractOpenApiVisitor.testReference
        then:
            openAPI.paths['/api/persons'].get.tags
            openAPI.paths['/api/persons'].get.tags.contains 'persons'
        where:
            // language=java
            source = '''                
                package test;
                
                import io.micronaut.http.annotation.*;
                
                import java.util.List;
                import java.util.Map;
                
                @Controller
                interface MyClient {
                
                    @Get("/api/persons")
                    List persons();
                
                    @Get("/api/persons/{personNo}")
                    Map persons(@QueryValue String personNo);
                
                }
                
                @javax.inject.Singleton
                class MyBean {}
                '''.stripIndent()

    }
}


