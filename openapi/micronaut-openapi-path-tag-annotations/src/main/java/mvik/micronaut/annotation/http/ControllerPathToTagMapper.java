package mvik.micronaut.annotation.http;

import io.micronaut.http.annotation.Controller;

public class ControllerPathToTagMapper extends AbstractPathToTagMapper<Controller> {

    @Override public Class<Controller> annotationType() {
        return Controller.class;
    }
}
