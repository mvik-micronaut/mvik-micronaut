package mvik.micronaut.annotation.http;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.processing.SupportedOptions;
import javax.validation.constraints.NotEmpty;

import io.micronaut.context.env.DefaultPropertyPlaceholderResolver;
import io.micronaut.context.env.PropertyPlaceholderResolver;
import io.micronaut.core.annotation.AnnotationValue;
import io.micronaut.core.convert.DefaultConversionService;
import io.micronaut.core.type.Argument;
import io.micronaut.core.value.MapPropertyResolver;
import io.micronaut.inject.annotation.TypedAnnotationMapper;
import io.micronaut.inject.visitor.VisitorContext;
import io.swagger.v3.oas.annotations.tags.Tag;

import static mvik.micronaut.annotation.http.AbstractPathToTagMapper.MICRONAUT_OPENAPI_PATH_TAG_MARKER;
import static mvik.micronaut.annotation.http.AbstractPathToTagMapper.MICRONAUT_OPENAPI_PATH_TAG_OVERRIDES;

/**
 * Adds a <code>@Tag</code> annotation for the first path element found after an <code>/api</code> element.
 */
@SupportedOptions({
    MICRONAUT_OPENAPI_PATH_TAG_MARKER,
    MICRONAUT_OPENAPI_PATH_TAG_OVERRIDES
})
public abstract class AbstractPathToTagMapper<T extends Annotation> implements TypedAnnotationMapper<T> {

    static final String MICRONAUT_OPENAPI_PATH_TAG_MARKER = "micronaut.openapi.path.tag.marker";
    static final String MICRONAUT_OPENAPI_PATH_TAG_OVERRIDES = "micronaut.openapi.path.tag.overrides";

    private static final String PATH_MATCHER_PATTERN_PROPERTY = "path.matcher.pattern";
    private static final String TAG_MARKER_DEFAULT = "api";

    private PropertyPlaceholderResolver propertyPlaceholderResolver;

    @Override public List<AnnotationValue<?>> map(AnnotationValue<T> annotation, VisitorContext context) {

        String path = annotation.getValue(String.class).orElse(null);
        if (path == null || path.trim().isEmpty()) {
            return Collections.emptyList();
        }

        String resolvedPath = getPropertyPlaceholderResolver().resolvePlaceholders(path).orElse("/");
        String part = findPathPartToTag(resolvedPath, context);
        if (part == null) {
            return Collections.emptyList();
        }
        return Collections.singletonList(
            AnnotationValue.builder(Tag.class).member("name", part).build()
        );
    }

    private static String findPathPartToTag(@NotEmpty String path, VisitorContext context) {

        List<TagOverride> overrides = getTagOverrides(context);
        if (!overrides.isEmpty()) {
            Optional<String> overrideMatch = overrides.stream()
                .filter(override -> path.contains(override.getPathPart()))
                .map(override -> override.getTag())
                .findFirst();
            if (overrideMatch.isPresent()) {
                return overrideMatch.get();
            }
        }

        Pattern pattern = context.get(PATH_MATCHER_PATTERN_PROPERTY, Pattern.class)
            .orElseGet(() -> {
                String marker = stringProperty(MICRONAUT_OPENAPI_PATH_TAG_MARKER, TAG_MARKER_DEFAULT, context);
                Pattern p = Pattern.compile("(.*/)?" + marker + "/([^/?{]+).*");
                context.info("Adding OpenAPI path tags: matching api paths using regex: " + p);
                context.put(PATH_MATCHER_PATTERN_PROPERTY, p);
                return p;
            });
        Matcher matcher = pattern.matcher(path);
        if (matcher.matches()) {
            return matcher.group(2);
        }
        return null;
    }

    private static List<TagOverride> getTagOverrides(VisitorContext context) {
        return context.get(MICRONAUT_OPENAPI_PATH_TAG_OVERRIDES, Argument.listOf(TagOverride.class))
            .orElseGet(() -> {
                String value = System.getProperty(MICRONAUT_OPENAPI_PATH_TAG_OVERRIDES, "").trim();
                List<String> list = value.isEmpty() ? Collections.emptyList() : Arrays.asList(value.split(" *, *"));
                if (!list.isEmpty()) {
                    context.info("Adding OpenAPI path tags: matching api paths using overrides: " + list);
                }
                List<TagOverride> tagOverrides = list.stream()
                    .map(override -> TagOverride.of(override))
                    .collect(Collectors.toList());
                context.put(MICRONAUT_OPENAPI_PATH_TAG_OVERRIDES, tagOverrides);
                return tagOverrides;
            });
    }

    @SuppressWarnings("SameParameterValue")
    private static String stringProperty(String key, String fallback, VisitorContext visitorContext) {
        return visitorContext.get(key, String.class)
            .orElseGet(() -> {
                String value = System.getProperty(key, fallback);
                visitorContext.put(key, value);
                return value;
            });
    }

    /**
     * @return An Instance of {@link PropertyPlaceholderResolver} to resolve placeholders.
     */
    PropertyPlaceholderResolver getPropertyPlaceholderResolver() {
        if (this.propertyPlaceholderResolver == null) {
            this.propertyPlaceholderResolver = new DefaultPropertyPlaceholderResolver(
                new MapPropertyResolver(Collections.emptyMap()),
                new DefaultConversionService()
            );
        }
        return this.propertyPlaceholderResolver;
    }

}
