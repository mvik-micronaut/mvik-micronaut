package mvik.micronaut.annotation.http;

import io.micronaut.http.annotation.Put;

public class PutPathToTagMapper extends AbstractPathToTagMapper<Put> {

    @Override public Class<Put> annotationType() {
        return Put.class;
    }
}
