package mvik.micronaut.annotation.http;

import io.micronaut.http.annotation.Post;

public class PostPathToTagMapper extends AbstractPathToTagMapper<Post> {

    @Override public Class<Post> annotationType() {
        return Post.class;
    }
}
