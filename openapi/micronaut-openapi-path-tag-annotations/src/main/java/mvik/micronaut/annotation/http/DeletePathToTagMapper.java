package mvik.micronaut.annotation.http;

import io.micronaut.http.annotation.Delete;

public class DeletePathToTagMapper extends AbstractPathToTagMapper<Delete> {

    @Override public Class<Delete> annotationType() {
        return Delete.class;
    }
}
