package mvik.micronaut.annotation.http;

import io.micronaut.http.annotation.Patch;

public class PatchPathToTagMapper extends AbstractPathToTagMapper<Patch> {

    @Override public Class<Patch> annotationType() {
        return Patch.class;
    }
}
