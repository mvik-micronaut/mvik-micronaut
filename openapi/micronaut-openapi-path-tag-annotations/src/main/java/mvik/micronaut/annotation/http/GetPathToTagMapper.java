package mvik.micronaut.annotation.http;

import io.micronaut.http.annotation.Get;

public class GetPathToTagMapper extends AbstractPathToTagMapper<Get> {

    @Override public Class<Get> annotationType() {
        return Get.class;
    }
}
