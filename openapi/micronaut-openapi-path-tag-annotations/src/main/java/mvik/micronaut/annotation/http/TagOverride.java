package mvik.micronaut.annotation.http;

class TagOverride {

    private final String pathPart;
    private final String tag;

    private TagOverride(String pathPart, String tag) {
        this.pathPart = pathPart;
        this.tag = tag;
    }

    static TagOverride of(String override) {
        String[] split = override.split(":", 2);
        return new TagOverride(
            split[0],
            split.length == 1 ? split[0] : split[1]
        );
    }

    String getPathPart() {
        return pathPart;
    }

    String getTag() {
        return tag;
    }

    @Override public String toString() {
        return "TagOverride{" +
            "pathPart='" + pathPart + '\'' +
            ", tag='" + tag + '\'' +
            '}';
    }
}
