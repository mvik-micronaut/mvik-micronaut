package mvik.demo.api;

import java.util.List;
import java.util.UUID;

import lombok.Data;

@Data
public class Person {

    private UUID uuid;
    private String name;
    private List<String> hobbies;
}
