package mvik.micronaut;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
    info = @Info(
        title = "demo-api",
        version = "0.1"
    )
)
public class OpenApiApp {

    static void main(String[] args) {
        Micronaut.run(OpenApiApp.class);
    }
}
