package mvik.micronaut.client;

import java.time.Instant;

import lombok.Data;

@Data
public class Weapon {

    private String name;
    private Instant createdAt;
    private Strength strength;
}
