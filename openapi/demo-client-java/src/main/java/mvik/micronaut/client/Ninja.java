package mvik.micronaut.client;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import lombok.Data;

@Data
public class Ninja {

    private UUID uuid;
    private String name;
    private Instant bornAt;
    private List<Weapon> weapons;
}

