package mvik.micronaut.client;

public enum Strength {
    Normal,
    Strong,
    Extreme
}
