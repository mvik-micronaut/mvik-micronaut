package mvik.micronaut.client;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.http.client.annotation.Client;

@Client("mega-service")
public interface WeaponClient {

    @Get("/api/weapons?ninjaUuid={ninjaUuid}")
    List<Weapon> findWeaponsByNinjaUuid(@QueryValue UUID ninjaUuid);

    @Get("/api/weapons?strength={strength}")
    List<Weapon> findWeaponsByStrength(@QueryValue Strength strength);

    @Post("/proxy/weapons")
    Map createWeaponByProxy(@Body Map body);
}
