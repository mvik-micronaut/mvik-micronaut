package mvik.micronaut.client;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.http.client.annotation.Client;

/**
 * Query and manipulate ninjas.
 */
@Client(id = "mega-service", path = "/api/ninjas")
public interface NinjaClient {

    /**
     * Retrieve all ninjas
     *
     * @return list of ninjas.
     */
    @Get
    List<Ninja> getNinjas();

    @Get("/{uuid}")
    Optional<Ninja> findNinja(@QueryValue UUID uuid);

    @Get("/{uuid}/weapons")
    List<Weapon> findWeaponsByNinjaUuid(@QueryValue UUID uuid);

    @Post
    Ninja create(@Body Ninja ninja);
}
