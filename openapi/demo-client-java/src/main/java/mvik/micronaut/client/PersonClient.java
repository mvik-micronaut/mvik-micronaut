package mvik.micronaut.client;

import java.util.List;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;
import mvik.demo.api.Person;

@Client(id = "mega-service", path = "/api/persons")
public interface PersonClient {

    @Get
    List<Person> getPersons();

    @Post
    Person createPerson(@Body Person person);
}
